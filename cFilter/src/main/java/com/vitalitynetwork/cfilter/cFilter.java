package com.vitalitynetwork.cfilter;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChatEvent;
import org.bukkit.plugin.java.JavaPlugin;

@SuppressWarnings("deprecation")
public final class cFilter extends JavaPlugin implements Listener {

    FileConfiguration config;
    boolean enabled = true;
    boolean censor = false;
    List<String> filterList = Arrays.asList("filtered", "messages", "go", "here");

    String chatClearedMessage = "Chat cleared";
    String chatClearedAllMessage = "The chat has been cleared by an Administrator.";

    String chatLockedMessageAttempted = "The chat is currently locked!";
    String chatLockedMessage = "You have {lock-status} the chat.";
    String chatLockedGlobalMessage = "The chat has been {lock-status}.";
    boolean chatLocked = false;

    int currentMessage = 0;

    @Override
    public void onEnable() {

	try {
	    config = getConfig();
	    File welcomeConfig = new File("plugins" + File.separator + "cFilter" + File.separator + "config.yml");
	    welcomeConfig.mkdir();

	    if (!config.contains("general.enabled")) {
		config.set("general.enabled", true);
		saveConfig();
	    } else {
		enabled = config.getBoolean("general.enabled");
	    }

	    if (!config.contains("general.censor-or-prevent-messages")) {
		config.set("general.censor-or-prevent-messages", "prevent");
		saveConfig();
	    } else {
		censor = config.getString("general.censor-or-prevent-messages").equalsIgnoreCase("prevent") ? false
			: true;
	    }

	    if (!config.contains("filter-list")) {
		config.set("filter-list", filterList);
		saveConfig();
	    } else {
		filterList = config.getStringList("filter-list");
	    }

	    if (!config.contains("chatlock-message")) {
		config.set("chatlock-message", chatLockedMessage);
		saveConfig();
	    } else {
		chatLockedMessage = ChatColor.translateAlternateColorCodes('&',
			config.getString("chatlock-message").replaceAll("#", ""));
	    }

	    if (!config.contains("chatlock-global-message")) {
		config.set("chatlock-global-message", chatLockedGlobalMessage);
		saveConfig();
	    } else {
		chatLockedGlobalMessage = ChatColor.translateAlternateColorCodes('&',
			config.getString("chatlock-global-message").replaceAll("#", ""));
	    }

	    if (!config.contains("chatlock-attempt-message")) {
		config.set("chatlock-attempt-message", chatLockedMessageAttempted);
		saveConfig();
	    } else {
		chatLockedMessageAttempted = ChatColor.translateAlternateColorCodes('&',
			config.getString("chatlock-attempt-message").replaceAll("#", ""));
	    }

	    if (!config.contains("clear-chat-message")) {
		config.set("clear-chat-message", chatClearedMessage);
		saveConfig();
	    } else {
		chatClearedMessage = ChatColor.translateAlternateColorCodes('&',
			config.getString("clear-chat-message").replaceAll("#", ""));
	    }

	    if (!config.contains("clear-chat-all-message")) {
		config.set("clear-chat-all-message", chatClearedAllMessage);
		saveConfig();
	    } else {
		chatClearedAllMessage = ChatColor.translateAlternateColorCodes('&',
			config.getString("clear-chat-all-message").replaceAll("#", ""));
	    }

	} catch (Exception e1) {
	    e1.printStackTrace();
	}

	getServer().getPluginManager().registerEvents(this, this);
	getLogger().info("cFilter has been started!");
	getLogger().info("Messages will " + (censor ? "" : "not ") + " be censored"
		+ (censor ? "." : ", they will be blocked."));

	this.getCommand("lockchat").setExecutor(new CommandExecutor() {
	    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		Player p = Bukkit.getPlayer(sender.getName());

		chatLocked = !chatLocked;
		String lockStatus = chatLocked ? "locked" : "unlocked";

		p.sendMessage(chatLockedMessage.replace("{lock-status}", lockStatus));

		if (chatLockedGlobalMessage.trim().length() > 0) {
		    Bukkit.broadcastMessage(chatLockedGlobalMessage.replace("{lock-status}", lockStatus));
		}

		return true;
	    }
	});

	this.getCommand("cls").setExecutor(new CommandExecutor() {
	    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		Player p = Bukkit.getPlayer(sender.getName());
		if (args.length > 0) {
		    if (args[0].equalsIgnoreCase("all")) {
			if (p.hasPermission("cfilter.clearscreen.all")) {
			    for (Player player : Bukkit.getOnlinePlayers()) {
				for (int i = 0; i < 100; i++) {
				    player.sendMessage("");
				}
				player.sendMessage(chatClearedAllMessage);
			    }
			    return true;
			}
		    }
		}
		for (int i = 0; i < 100; i++) {
		    p.sendMessage("");
		}
		p.sendMessage(chatClearedMessage);
		return true;
	    }
	});
    }

    @Override
    public void onDisable() {
	getLogger().info("cFilter has been disabled!");
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onSendMessage(PlayerChatEvent event) {
	if (event.getPlayer().hasPermission("cfilter.bypassall")) {
	    return;
	}
	if (enabled) {
	    if (chatLocked) {
		if (!event.getPlayer().hasPermission("cfilter.lockchat.bypass")) {
		    event.getPlayer().sendMessage(chatLockedMessageAttempted);
		    event.setCancelled(true);
		}
	    }
	    if (!event.getPlayer().hasPermission("cfilter.filter.bypass")) {
		if (censor) {
		    event.setMessage(censorMessage(event.getMessage()));
		} else {
		    if (messageContainsBannedWord(event.getMessage())) {
			event.setCancelled(true);
			System.out.println("Blocked a message sent by " + event.getPlayer().getDisplayName());
		    }
		}
	    }
	}
    }

    private boolean messageContainsBannedWord(String message) {
	for (String word : filterList) {
	    if (message.toLowerCase().contains(word.toLowerCase())) {
		return true;
	    }
	}
	return false;
    }

    private String censorMessage(String message) {
	String toCensor = message;
	for (String word : filterList) {
	    toCensor = toCensor.replaceAll("(?i)" + word, getStars(word)); // (?i)
	    // to
	    // ignore
	    // case
	}
	return toCensor;
    }

    private String getStars(String word) {
	String stars = "";
	for (int i = 0; i < word.length(); i++) {
	    stars += "*";
	}
	return stars;
    }

}