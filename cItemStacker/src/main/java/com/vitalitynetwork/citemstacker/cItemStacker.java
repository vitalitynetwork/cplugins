package com.vitalitynetwork.citemstacker;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

public class cItemStacker extends JavaPlugin implements Listener {

    @Override
    public void onEnable() {
	getLogger().info(this.getClass().getSimpleName() + " has been started!");
	getServer().getPluginManager().registerEvents(this, this);
    }

    @Override
    public void onDisable() {
	getLogger().info("Disabling " + this.getClass().getSimpleName());
    }

    @EventHandler
    public void onItemPickup(PlayerPickupItemEvent event) {
	
	Player p = event.getPlayer();
	
	if (event.getItem().getItemStack().getType() == Material.LAVA_BUCKET) {
	    p.sendMessage("Picked up " + event.getItem().getItemStack().getAmount() + " lava buckets");
	    if (p.getInventory().contains(Material.LAVA_BUCKET)) {

		int amount = 0;
		int currentAmount = 0;

		for (ItemStack inventoryItem : p.getInventory().getContents()) {
		    if (inventoryItem != null) {
			if (inventoryItem.getType() == Material.LAVA_BUCKET) {
			    amount = inventoryItem.getAmount() + event.getItem().getItemStack().getAmount();
			    currentAmount += inventoryItem.getAmount();
			    break;
			}
		    }
		}

		@SuppressWarnings("deprecation")
		ItemStack item = new ItemStack(Material.LAVA_BUCKET.getId(), amount, (byte) 0);
		p.getInventory().removeItem(new ItemStack(Material.LAVA_BUCKET.getId(), currentAmount, (byte) 0));
		p.getInventory().addItem(item);

		event.getItem().remove();
		
		event.setCancelled(true);

	    }
	} else {
	    //event.getPlayer().sendMessage("nah sorry lad");
	}
    }
    
    
    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {

	Player p = Bukkit.getPlayer(event.getWhoClicked().getName());
	
	ItemStack clickedItem = event.getCursor();
	ItemStack inHand = event.getCurrentItem();
	
	if (clickedItem.getType() != Material.LAVA_BUCKET || inHand.getType() != Material.LAVA_BUCKET) {
	    return;
	}
	
	int amount = clickedItem.getAmount() + inHand.getAmount();
	
	System.out.println(clickedItem.getType().name() + " x" + clickedItem.getAmount() + " = clicked item");
	System.out.println(inHand.getType().name() + " x" + inHand.getAmount() + " = cursor item");
	
	//event.setCursor(null);
	//event.setCurrentItem(null);
	
	inHand.setAmount(0);
	clickedItem.setAmount(amount);
	
	p.updateInventory();
	
	
	/*int amount = event.getCursor().getAmount() + event.getCurrentItem().getAmount();
	
	@SuppressWarnings("deprecation")
	ItemStack item = new ItemStack(Material.LAVA_BUCKET.getId(), amount, (byte) 0);
	ItemStack air = new ItemStack(Material.AIR.getId(), 0, (byte) 0);
	
	System.out.println(event.getCurrentItem().getType().name());
	//System.out.println(event.getRawSlot());
	//System.out.println(event.getSlot());
	event.setCursor(air);
	event.setCurrentItem(item);*/
	
	//event.setCancelled(true);
	
    }
    
    @EventHandler
    public void onInventoryEvent(InventoryEvent event) {
	System.out.println(event.getEventName());
    }

}
