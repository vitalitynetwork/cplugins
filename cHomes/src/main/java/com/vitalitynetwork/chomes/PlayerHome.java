package com.vitalitynetwork.chomes;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.util.NumberConversions;

public class PlayerHome implements ConfigurationSerializable {

    private String name;
    private World world;
    private double xCoord;
    private double yCoord;
    private double zCoord;
    private float yaw;
    private float pitch;

    public PlayerHome(String name, World world, double xCoord, double yCoord, double zCoord, float yaw, float pitch) {
	this.name = name;
	this.world = world;
	this.xCoord = xCoord;
	this.yCoord = yCoord;
	this.zCoord = zCoord;
	this.yaw = yaw;
	this.pitch = pitch;
    }

    public PlayerHome(String name, World world, Location loc) {
	this.name = name;
	this.world = world;
	this.xCoord = loc.getX();
	this.yCoord = loc.getY();
	this.zCoord = loc.getZ();
	this.yaw = loc.getYaw();
	this.pitch = loc.getPitch();
    }

    public String getName() {
	return name;
    }

    public World getWorld() {
	return world;
    }

    public double getXCoord() {
	return xCoord;
    }

    public double getYCoord() {
	return yCoord;
    }

    public double getZCoord() {
	return zCoord;
    }
    
    public float getYaw() {
	return yaw;
    }

    public float getPitch() {
	return pitch;
    }

    public Location getLocation() {
	return new Location(world, xCoord, yCoord, zCoord, yaw, pitch);
    }

    @Override
    public Map<String, Object> serialize() {
	Map<String, Object> data = new HashMap<String, Object>();
	data.put("name", this.name);

	data.put("world", this.world.getName());

	data.put("x", this.xCoord);
	data.put("y", this.yCoord);
	data.put("z", this.zCoord);
	
	data.put("yaw", this.yaw);
	data.put("pitch", this.pitch);

	return data;
    }
    
    @Override
    public String toString() {
	DecimalFormat df = new DecimalFormat();
	df.setMaximumFractionDigits(2);
	return df.format(xCoord) + ", " + df.format(yCoord) + ", " + df.format(zCoord);
    }
    
    public static PlayerHome deserialize(Map<String, Object> args) {
	World world = Bukkit.getWorld((String) args.get("world"));
	if (world == null) {
		throw new IllegalArgumentException("unknown world");
	}

	return new PlayerHome(args.get("name") + "", world, NumberConversions.toDouble(args.get("x")),
		NumberConversions.toDouble(args.get("y")), NumberConversions.toDouble(args.get("z")),
		NumberConversions.toFloat(args.get("yaw")), NumberConversions.toFloat(args.get("pitch")));
    }

}
