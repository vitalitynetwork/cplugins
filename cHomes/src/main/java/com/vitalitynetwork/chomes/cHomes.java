package com.vitalitynetwork.chomes;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class cHomes extends JavaPlugin implements Listener {

    private static final int TICK_DELAY = 20;

    FileConfiguration config;
    Boolean enabled = true;

    String teleportMessage = "Teleporting to {home name}";
    String teleportDelayMessage = "Teleport will commence in {teleport-delay-in-seconds} seconds, don't move.";
    String teleportCancelMessage = "You moved! Teleportation cancelled.";
    int teleportDelayInSeconds = 5;

    long teleportTickDelay = teleportDelayInSeconds * TICK_DELAY;

    HashMap<UUID, Integer> map = new HashMap<UUID, Integer>();

    @Override
    public void onEnable() {
	ConfigurationSerialization.registerClass(PlayerHome.class);
	Bukkit.getServer().getPluginManager().registerEvents(this, this);

	try {
	    config = getConfig();
	    File welcomeConfig = new File("plugins" + File.separator + "cHomes" + File.separator + "config.yml");
	    welcomeConfig.mkdir();

	    if (!config.contains("general.enabled")) {
		config.set("general.enabled", true);
		saveConfig();
	    } else {
		enabled = config.getBoolean("general.enabled");
	    }

	    if (!config.contains("teleport.message")) {
		config.set("teleport.message", teleportMessage);
		saveConfig();
	    } else {
		teleportMessage = ChatColor.translateAlternateColorCodes('&',
			config.getString("teleport.message").replaceAll("#", ""));
	    }

	    if (!config.contains("teleport.delay_message")) {
		config.set("teleport.delay_message", teleportDelayMessage);
		saveConfig();
	    } else {
		teleportDelayMessage = ChatColor.translateAlternateColorCodes('&',
			config.getString("teleport.delay_message").replaceAll("#", ""));
	    }

	    if (!config.contains("teleport.cancel_message")) {
		config.set("teleport.cancel_message", teleportCancelMessage);
		saveConfig();
	    } else {
		teleportCancelMessage = ChatColor.translateAlternateColorCodes('&',
			config.getString("teleport.cancel_message").replaceAll("#", ""));
	    }

	    if (!config.contains("teleport.delay-in-seconds")) {
		config.set("teleport.delay-in-seconds", teleportDelayInSeconds);
		saveConfig();
	    } else {
		teleportDelayInSeconds = config.getInt("teleport.delay-in-seconds");
	    }

	} catch (Exception e1) {
	    e1.printStackTrace();
	}

	getLogger().info("cHomes has been started!");

	this.getCommand("sethome").setExecutor(new CommandExecutor() {

	    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (enabled) {
		    Player p = Bukkit.getPlayer(sender.getName());
		    File customConfigFile = new File("plugins" + File.separator + "cHomes" + File.separator + "players"
			    + File.separator + p.getName() + ".yml");
		    FileConfiguration customConfig = YamlConfiguration.loadConfiguration(customConfigFile);

		    if (!customConfigFile.exists()) {
			try {
			    customConfigFile.createNewFile();
			} catch (IOException e) {
			}
		    }

		    HashMap<String, PlayerHome> playerHomes = loadHomes(customConfig);

		    String homeName = getHomeName(args);
		    if (homeName.contains(":")) {
			p.sendMessage(ChatColor.DARK_RED + "Could not set home!");
			p.sendMessage(ChatColor.DARK_RED + "Illegal characters in home name!");
			return false;
		    }

		    if (!playerHomes.containsKey(homeName)) {
			if (!setNewHome(p, p.getWorld(), homeName, p.getLocation(), playerHomes, customConfig)) {
			    return false;
			}
		    } else {
			p.sendMessage(ChatColor.RED + "Home '" + homeName + "' already exists!");
			p.sendMessage(ChatColor.RED + "Delete it first before setting it again.");
			return false;
		    }

		    try {
			customConfig.save(customConfigFile);
		    } catch (IOException e) {
			getLogger().severe("Failed to save home for player " + p.getName());
			e.printStackTrace();
			return false;
		    }
		} else {
		    sender.sendMessage(ChatColor.DARK_RED + "cHomes not enabled! Enable in config.yml");
		}
		return true;
	    }
	});

	this.getCommand("home").setExecutor(new CommandExecutor() {

	    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (enabled) {
		    Player p = Bukkit.getPlayer(sender.getName());
		    File customConfigFile = new File("plugins" + File.separator + "cHomes" + File.separator + "players"
			    + File.separator + p.getName() + ".yml");
		    FileConfiguration customConfig = YamlConfiguration.loadConfiguration(customConfigFile);

		    String homeName = getHomeName(args);

		    if (args.length > 0) {
			if (args[args.length - 1].contains("other:")) {
			    if (p.hasPermission("cHomes.home.other")) {

				// the last argument should always be the
				// username
				String userOther = args[args.length - 1].replaceAll("other:", "");

				customConfigFile = new File("plugins" + File.separator + "cHomes" + File.separator
					+ "players" + File.separator + userOther + ".yml");
				customConfig = YamlConfiguration.loadConfiguration(customConfigFile);

				if (!customConfigFile.exists()) {
				    p.sendMessage(ChatColor.RED
					    + "Player doesn't exist or name has been typed incorrectly (case sensitive)");
				    p.sendMessage(ChatColor.RED + "The player may also not have any homes set.");
				    return false;
				}

			    } else {
				p.sendMessage(ChatColor.DARK_RED + "You do not have permission to use this command.");
				return false;
			    }
			}
		    }

		    HashMap<String, PlayerHome> playerHomes = loadHomes(customConfig);

		    if (playerHomes.containsKey(homeName)) {
			startTeleport(p, playerHomes.get(homeName));
			return true;
		    } else {
			p.sendMessage(ChatColor.RED + "Home '" + homeName + "' doesn't exist!");
			return false;
		    }
		} else {
		    sender.sendMessage(ChatColor.DARK_RED + "cHomes not enabled! Enable in config.yml");
		    return false;
		}
	    }
	});

	this.getCommand("homes").setExecutor(new CommandExecutor() {

	    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (enabled) {
		    Player p = Bukkit.getPlayer(sender.getName());
		    File customConfigFile = new File("plugins" + File.separator + "cHomes" + File.separator + "players"
			    + File.separator + p.getName() + ".yml");
		    FileConfiguration customConfig = YamlConfiguration.loadConfiguration(customConfigFile);

		    if (args.length > 0) {
			if (args.length > 0) {
			    if (p.hasPermission("cHomes.homes.other")) {
				String homeOther = args[args.length - 1].replaceAll("other:", "");

				if (Bukkit.getPlayer(homeOther).isOnline()) {
				    homeOther = Bukkit.getPlayer(homeOther).getName();
				}

				customConfigFile = new File("plugins" + File.separator + "cHomes" + File.separator
					+ "players" + File.separator + homeOther + ".yml");
				customConfig = YamlConfiguration.loadConfiguration(customConfigFile);

				if (!customConfigFile.exists()) {
				    p.sendMessage(ChatColor.RED
					    + "Player doesn't exist or name has been typed incorrectly (case sensitive)");
				    p.sendMessage(ChatColor.RED + "The player may also not have any homes set.");
				    return false;
				}

			    } else {
				p.sendMessage(ChatColor.DARK_RED + "You do not have permission to use this command.");
				return false;
			    }
			}
		    }

		    HashMap<String, PlayerHome> playerHomes = loadHomes(customConfig);

		    if (playerHomes.size() < 1) {
			p.sendMessage(ChatColor.RED + "You do not have any homes set.");
			return true;
		    }

		    int homeCount = 1;
		    p.sendMessage(ChatColor.YELLOW + "List of homes:");
		    for (PlayerHome home : playerHomes.values()) {
			p.sendMessage(ChatColor.YELLOW + "" + (homeCount++) + ") " + ChatColor.RED + home.getName()
				+ ChatColor.YELLOW + " - " + ChatColor.RED + home.toString());
		    }
		    return true;
		} else {
		    sender.sendMessage(ChatColor.DARK_RED + "cHomes not enabled! Enable in config.yml");
		    return false;
		}
	    }
	});

	this.getCommand("delhome").setExecutor(new CommandExecutor() {

	    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (enabled) {
		    Player p = Bukkit.getPlayer(sender.getName());
		    File customConfigFile = new File("plugins" + File.separator + "cHomes" + File.separator + "players"
			    + File.separator + p.getName() + ".yml");
		    FileConfiguration customConfig = YamlConfiguration.loadConfiguration(customConfigFile);

		    HashMap<String, PlayerHome> playerHomes = loadHomes(customConfig);

		    customConfigFile.delete();
		    customConfig = YamlConfiguration.loadConfiguration(customConfigFile);

		    String homeName = getHomeName(args);

		    if (args.length > 0) {
			if (args[args.length - 1].contains("other:")) {
			    if (p.hasPermission("cHomes.delhome.other")) {

				// the last argument should always be the
				// username
				String userOther = args[args.length - 1].replaceAll("other:", "");

				customConfigFile = new File("plugins" + File.separator + "cHomes" + File.separator
					+ "players" + File.separator + userOther + ".yml");
				customConfig = YamlConfiguration.loadConfiguration(customConfigFile);

				if (!customConfigFile.exists()) {
				    p.sendMessage(ChatColor.RED
					    + "Player doesn't exist or name has been typed incorrectly (case sensitive)");
				    p.sendMessage(ChatColor.RED + "The player may also not have any homes set.");
				    return false;
				}

				playerHomes = loadHomes(customConfig);

			    } else {
				p.sendMessage(ChatColor.DARK_RED + "You do not have permission to use this command.");
				return false;
			    }
			}
		    }

		    if (playerHomes.containsKey(homeName)) {
			playerHomes.remove(homeName);
			saveHomes(playerHomes, customConfig);
			try {
			    customConfig.save(customConfigFile);
			    p.sendMessage(ChatColor.GOLD + "Successfully deleted home '" + homeName + "'");
			    return true;
			} catch (IOException e) {
			    getLogger().severe("Failed to delete home for player " + p.getName());
			    e.printStackTrace();
			    return false;
			}
		    } else {
			p.sendMessage(ChatColor.RED + "Home '" + homeName + "' doesn't exist!");
			return false;
		    }
		} else {
		    sender.sendMessage(ChatColor.DARK_RED + "cHomes not enabled! Enable in config.yml");
		    return false;
		}
	    }
	});

    }

    @Override
    public void onDisable() {
	getLogger().info("Disabling cHomes");
    }

    @EventHandler
    public void onLogout(PlayerQuitEvent event) {
	Player player = event.getPlayer();
	if (map.containsKey(player.getUniqueId())) {
	    Bukkit.getScheduler().cancelTask(map.get(player.getUniqueId()));
	    map.remove(player.getUniqueId());
	}
    }

    // https://bukkit.org/threads/teleport-player-after-5-seconds-of-not-moving.369911/
    @EventHandler
    public void onMove(PlayerMoveEvent event) {
	Player player = event.getPlayer();
	if (map.containsKey(player.getUniqueId())) {
	    if (round(event.getTo().getX(), 1) != round(event.getFrom().getX(), 1)
		    || event.getTo().getY() != event.getFrom().getY()
		    || round(event.getTo().getZ(), 1) != round(event.getFrom().getZ(), 1)) {
		Bukkit.getScheduler().cancelTask(map.get(player.getUniqueId()));
		map.remove(player.getUniqueId());
		player.sendMessage(teleportCancelMessage);
	    }
	}
    }

    // https://bukkit.org/threads/teleport-player-after-5-seconds-of-not-moving.369911/
    private void startTeleport(final Player p, final PlayerHome home) {
	if (p.hasPermission("chomes.bypass-teleport-delay")) {
	    p.teleport(home.getLocation());
	    p.sendMessage(teleportMessage.replace("{home name}", home.getName()));
	    return;
	}
	p.sendMessage(teleportDelayMessage.replace("{teleport-delay-in-seconds}", teleportDelayInSeconds + ""));
	int taskId = Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
	    @Override
	    public void run() {
		p.teleport(home.getLocation());
		p.sendMessage(teleportMessage.replace("{home name}", home.getName()));
		map.remove(p.getUniqueId());
		return;
	    }
	}, teleportTickDelay);
	map.put(p.getUniqueId(), taskId);
    }

    private HashMap<String, PlayerHome> loadHomes(FileConfiguration customConfig) {
	HashMap<String, PlayerHome> playerHomes = new HashMap<String, PlayerHome>();
	if (customConfig.getConfigurationSection("homes") != null) {
	    for (String key : customConfig.getConfigurationSection("homes").getKeys(false)) {
		playerHomes.put(key.replace("_", " "),
			PlayerHome.deserialize(customConfig.getConfigurationSection("homes." + key).getValues(true)));
	    }
	}
	return playerHomes;
    }

    private void saveHomes(HashMap<String, PlayerHome> playerHomes, FileConfiguration customConfig) {
	for (PlayerHome home : playerHomes.values()) {
	    customConfig.createSection("homes." + home.getName().replace(" ", "_"), home.serialize());
	}
    }

    private boolean setNewHome(Player p, World world, String name, Location loc,
	    HashMap<String, PlayerHome> playerHomes, FileConfiguration customConfig) {
	try {
	    if (canMakeHome(p, playerHomes)) {
		PlayerHome newHome = new PlayerHome(name, world, loc);
		playerHomes.put(name, newHome);
		saveHomes(playerHomes, customConfig);
		p.sendMessage(ChatColor.YELLOW + "You've set a new home at (" + ChatColor.RED + newHome.toString()
			+ ChatColor.YELLOW + ") named " + ChatColor.RED + name);
	    } else {
		p.sendMessage(ChatColor.RED + "You have too many homes set!");
		p.sendMessage(ChatColor.RED + "Try removing some first.");
		return true;
	    }
	} catch (Exception c) {
	    p.sendMessage(ChatColor.RED + "Failed to set home");
	    c.printStackTrace();
	    return false;
	}
	return true;
    }

    private String getHomeName(String[] args) {
	String homeName = "";
	for (int i = 0; i < args.length; i++) {
	    if (!args[i].contains("other:")) {
		homeName += args[i] + " ";
	    }
	}
	homeName = homeName.trim();
	if (homeName.length() == 0) {
	    homeName = "default";
	}
	return homeName;
    }

    private boolean canMakeHome(Player p, HashMap<String, PlayerHome> homes) {
	return getHomesMade(homes) < getMaxHomesAllowed(p);
    }

    private int getHomesMade(HashMap<String, PlayerHome> homes) {
	return homes.size();
    }

    // TODO make this less cancerous
    private int getMaxHomesAllowed(Player p) {
	if (p.hasPermission("cHomes.homes.number.0")) {
	    return 0;
	} else if (p.hasPermission("cHomes.homes.number.1")) {
	    return 1;
	} else if (p.hasPermission("cHomes.homes.number.2")) {
	    return 2;
	} else if (p.hasPermission("cHomes.homes.number.3")) {
	    return 3;
	} else if (p.hasPermission("cHomes.homes.number.4")) {
	    return 4;
	} else if (p.hasPermission("cHomes.homes.number.5")) {
	    return 5;
	} else if (p.hasPermission("cHomes.homes.number.6")) {
	    return 6;
	} else if (p.hasPermission("cHomes.homes.number.7")) {
	    return 7;
	} else if (p.hasPermission("cHomes.homes.number.8")) {
	    return 8;
	} else if (p.hasPermission("cHomes.homes.number.9")) {
	    return 9;
	} else if (p.hasPermission("cHomes.homes.number.10")) {
	    return 10;
	} else if (p.hasPermission("cHomes.homes.number.11")) {
	    return 11;
	} else if (p.hasPermission("cHomes.homes.number.12")) {
	    return 12;
	} else if (p.hasPermission("cHomes.homes.number.13")) {
	    return 13;
	} else if (p.hasPermission("cHomes.homes.number.14")) {
	    return 14;
	} else if (p.hasPermission("cHomes.homes.number.15")) {
	    return 15;
	} else if (p.hasPermission("cHomes.homes.number.16")) {
	    return 16;
	} else if (p.hasPermission("cHomes.homes.number.17")) {
	    return 17;
	} else if (p.hasPermission("cHomes.homes.number.18")) {
	    return 18;
	} else if (p.hasPermission("cHomes.homes.number.19")) {
	    return 19;
	} else if (p.hasPermission("cHomes.homes.number.20")) {
	    return 20;
	} else if (p.hasPermission("cHomes.homes.number.unlim") || p.hasPermission("cHomes.homes.number.unlimited")) {
	    return 1000;
	} else {
	    return 1;
	}
    }

    public static double round(double value, int places) {
	if (places < 0)
	    throw new IllegalArgumentException();

	BigDecimal bd = new BigDecimal(value);
	bd = bd.setScale(places, RoundingMode.HALF_UP);
	return bd.doubleValue();
    }

}
