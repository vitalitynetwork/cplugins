package com.vitalitynetwork.cjoinmessage;

import java.io.File;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class cJoinMessage extends JavaPlugin implements Listener {

    FileConfiguration config;
    String joinMessage = "[+]{user}";
    String leaveMessage = "[-]{user}";
    String newPlayerMessage = "{user} has joined {server name} for the first time and is the {unique player count}st player to ever explore this network!";
    String serverName = "VitalityNetwork";
    String personalJoinMessage = "You are player #{player count} online!";
    int uniquePlayers = 0;

    @Override
    public void onEnable() {

	try {
	    config = getConfig();
	    File welcomeConfig = new File("plugins" + File.separator + "cJoinMessage" + File.separator + "config.yml");
	    welcomeConfig.mkdir();

	    if (!config.contains("general.login.join-message")) {
		config.set("general.login.join-message", joinMessage);
		saveConfig();
	    } else {
		joinMessage = ChatColor
			.translateAlternateColorCodes('&', config.getString("general.login.join-message")).trim();
	    }

	    if (!config.contains("general.login.leave-message")) {
		config.set("general.login.leave-message", leaveMessage);
		saveConfig();
	    } else {
		leaveMessage = ChatColor
			.translateAlternateColorCodes('&', config.getString("general.login.leave-message")).trim();
	    }

	    if (!config.contains("general.login.new-player-message")) {
		config.set("general.login.new-player-message", newPlayerMessage);
		saveConfig();
	    } else {
		newPlayerMessage = ChatColor
			.translateAlternateColorCodes('&', config.getString("general.login.new-player-message")).trim();
	    }

	    if (!config.contains("general.login.personal-join-message")) {
		config.set("general.login.personal-join-message", personalJoinMessage);
		saveConfig();
	    } else {
		personalJoinMessage = ChatColor
			.translateAlternateColorCodes('&', config.getString("general.login.personal-join-message"))
			.trim();
	    }

	    if (!config.contains("general.server-name")) {
		config.set("general.server-name", serverName);
		saveConfig();
	    } else {
		serverName = ChatColor.translateAlternateColorCodes('&', config.getString("general.server-name"))
			.trim();
	    }

	    if (!config.contains("general.unique-player-count")) {
		config.set("general.unique-player-count", uniquePlayers);
		saveConfig();
	    } else {
		uniquePlayers = config.getInt("general.unique-player-count");
	    }

	} catch (Exception e1) {
	    e1.printStackTrace();
	}

	getLogger().info("cJoinMessage has been started!");
	getServer().getPluginManager().registerEvents(this, this);
    }

    @Override
    public void onDisable() {
	getLogger().info("Disabling cJoinMessage");
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onLogin(PlayerJoinEvent event) {
	OfflinePlayer p = event.getPlayer();
	getLogger().log(Level.INFO, "Player " + p.getName() + " is logging in!");

	if (personalJoinMessage.length() > 0) {
	    event.getPlayer()
		    .sendMessage(personalJoinMessage.replace("{player count}", Bukkit.getOnlinePlayers().size() + ""));
	}

	Bukkit.broadcastMessage(joinMessage.replace("{user}", p.getName()));

	if (!p.hasPlayedBefore()) {
	    incrementUniquePlayerCount();
	    Bukkit.broadcastMessage(newPlayerMessage.replace("{user}", p.getName()).replace("{server name}", serverName)
		    .replace("{unique player count}", uniquePlayers + ""));
	}
    }

    @EventHandler
    public void onLogout(PlayerQuitEvent event) {
	Player p = event.getPlayer();
	getLogger().log(Level.INFO, "Player " + p.getName() + " has logged out.");
	Bukkit.broadcastMessage(leaveMessage.replace("{user}", p.getName()));
    }

    private void incrementUniquePlayerCount() {
	uniquePlayers++;
	config.set("general.unique-player-count", uniquePlayers);
	saveConfig();
    }

}
