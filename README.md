# cPlugins: #
___
## Plugin contributors: ##

* **Corey** - IGN: BruceBanner

___
## Plugin overview: ##

* **cHomes** *" This plugin will basically be the same as essentials home, where you set your home,  and it will teleport you there. The problem is it will run into problems with Essentials as essentials already as this command however essentials home is really weird and tacty"*
* **cBroadcast** *"Every x amount of time it says "x" in chat"*
* **cJoinMessage** *"Shows each time a player joins the server"*
* **cFilter** *"Filters messages based on a list of words - can block messages or simply censor them"*

___
## Plugin details: ##

### cHomes: ###

* Text
* goes
* here

### cBroadcast: ###

* Text
* goes
* here

### cJoinMessage: ###

* Text
* goes
* here

### cFilter: ###

* Text
* goes
* here

___