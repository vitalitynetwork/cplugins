package com.vitalitynetwork.chandycommands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class cHandyCommands extends JavaPlugin {

    @Override
    public void onEnable() {
	getLogger().info(this.getClass().getSimpleName() + " has been started!");

	this.getCommand("nv").setExecutor(new CommandExecutor() {

	    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		Player p = Bukkit.getPlayer(sender.getName());
		if (!p.hasPotionEffect(PotionEffectType.NIGHT_VISION)) {
		    p.addPotionEffect(
			    new PotionEffect(PotionEffectType.NIGHT_VISION, Integer.MAX_VALUE, 0, true));
		    p.sendMessage(ChatColor.BLUE + "Night Vision enabled!");
		} else {
		    p.removePotionEffect(PotionEffectType.NIGHT_VISION);
		    p.sendMessage(ChatColor.BLUE + "Night Vision disabled!");
		}
		return true;
	    }
	});

    }

    @Override
    public void onDisable() {
	getLogger().info("Disabling " + this.getClass().getSimpleName());
    }

}
