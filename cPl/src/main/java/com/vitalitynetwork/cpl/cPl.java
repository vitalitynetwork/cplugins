package com.vitalitynetwork.cpl;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class cPl extends JavaPlugin {

    FileConfiguration config;
    String denyMessage = "# You do not have permission to do this command.";
    
    @Override
    public void onEnable() {
	
	try {
	    config = getConfig();
	    File welcomeConfig = new File("plugins" + File.separator + "cPl" + File.separator + "config.yml");
	    welcomeConfig.mkdir();

	    if (!config.contains("deny_message")) {
		config.set("deny_message", denyMessage);
		saveConfig();
	    } else {
		denyMessage = ChatColor.translateAlternateColorCodes('&', config.getString("deny_message").replaceAll("#", ""));
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
	
	getLogger().info(this.getClass().getSimpleName() + " has been started!");

	this.getCommand("pl").setExecutor(new CommandExecutor() {

	    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		Player p = Bukkit.getPlayer(sender.getName());
		if (p.hasPermission("cPl.show")) {
		    StringBuilder sb = new StringBuilder(ChatColor.YELLOW + "Plugins: ");
		    int pluginCount = 0;
		    for (Plugin plugin : Bukkit.getPluginManager().getPlugins()) {
			sb.append(ChatColor.GREEN + plugin.getName() + ChatColor.RED + ", ");
			pluginCount++;
		    }
		    String plugins = sb.toString().replaceAll(", $", "").trim();
		    plugins += (ChatColor.GREEN + " (" + ChatColor.YELLOW + pluginCount + ChatColor.GREEN + ")");
		    p.sendMessage(plugins);
		} else {
		    p.sendMessage(denyMessage.trim());
		}
		return true;
	    }
	});

    }

    @Override
    public void onDisable() {
	getLogger().info("Disabling " + this.getClass().getSimpleName());
    }

}
