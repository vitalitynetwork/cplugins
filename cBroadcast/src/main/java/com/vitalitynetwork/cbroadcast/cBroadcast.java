package com.vitalitynetwork.cbroadcast;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;

import org.bukkit.ChatColor;

public class cBroadcast extends JavaPlugin {

    final int ticksPerSecond = 20;
    final int defaultBroadcastDelay = 30;

    FileConfiguration config;
    Boolean enabled = true;
    Boolean random = false;
    long delay = (long) defaultBroadcastDelay * ticksPerSecond; // default to 30 seconds
    List<String> broadcastMessages = Arrays.asList("START:broadcast messages", "START:go", "START:here", "START:colour",
	    "START:codes", "START:are supported", "START:but you must start with 'START:'");

    int currentMessage = 0;

    @Override
    public void onEnable() {

	try {
	    config = getConfig();
	    File welcomeConfig = new File("plugins" + File.separator + "cBroadcast" + File.separator + "config.yml");
	    welcomeConfig.mkdir();

	    if (!config.contains("general.enabled")) {
		config.set("general.enabled", true);
		saveConfig();
	    } else {
		enabled = config.getBoolean("general.enabled");
	    }

	    if (!config.contains("general.random")) {
		config.set("general.random", false);
		saveConfig();
	    } else {
		random = config.getBoolean("general.random");
	    }

	    if (!config.contains("general.delay-in-seconds")) {
		config.set("general.delay-in-seconds", defaultBroadcastDelay);
		saveConfig();
	    } else {
		delay = (long) (config.getInt("general.delay-in-seconds") * ticksPerSecond);
	    }

	    if (!config.contains("broadcasts")) {
		config.set("broadcasts", broadcastMessages);
		saveConfig();
	    } else {
		broadcastMessages = config.getStringList("broadcasts");
	    }

	} catch (Exception e1) {
	    e1.printStackTrace();
	}

	if (enabled) {
	    BukkitScheduler scheduler = getServer().getScheduler();
	    scheduler.scheduleSyncRepeatingTask(this, new Runnable() {
		@Override
		public void run() {
		    getLogger().info("Sent broadcast to " + (Bukkit.broadcastMessage(getNextBroadcast()) - 1)
			    + " players online.");
		}
	    }, 0L, delay);
	}

	getLogger().info("cBroadcast has been started!");
    }

    @Override
    public void onDisable() {
	getLogger().info("cBroadcast has been disabled!");
    }

    private String getNextBroadcast() {
	if (random) {
	    Random rand = new Random();
	    int randomMessage = rand.nextInt(broadcastMessages.size());
	    // to make sure the next message isn't the same as the last one
	    while (randomMessage == currentMessage) {
		randomMessage = rand.nextInt(broadcastMessages.size());
	    }
	    currentMessage = randomMessage;
	    String message = broadcastMessages.get(randomMessage);
	    return ChatColor.translateAlternateColorCodes('&', message).replaceAll("START:", "");
	}

	String message = broadcastMessages.get(currentMessage);

	if (currentMessage != broadcastMessages.size() - 1) {
	    currentMessage++;
	} else {
	    message = broadcastMessages.get(currentMessage);
	    currentMessage = 0;
	}
	return ChatColor.translateAlternateColorCodes('&', message).replaceAll("START:", "");
    }
}